import React, { useEffect, useState } from "react";

import "./App.css";
import Header from "./components/Header";
import TrelloBoard from "./components/TrelloBoards";
import BoardLists from "./components/listsPage/BoardLists";
import { Stack } from "@mui/material";

import { Routes, Route } from "react-router-dom";
import ErrorPage from "./components/ErrorPage";
import Loader from "./components/Loader";
import ErrorDialog from "./components/ErrorDialog";

import { useSelector, useDispatch } from "react-redux";
import { fetchBoardData } from "./store/boardReducer";

import { SnackbarProvider, useSnackbar } from "notistack";

function App() {
  const boardData = useSelector((state) => {
    return state.board;
  });
  const dispatch = useDispatch();

  useEffect(() => {
  
      dispatch(fetchBoardData());
  
  }, []);

 
  return (
    <>
      <SnackbarProvider maxSnack={2}>
        <Header />
        <Routes>
          <Route
            path="/"
            element={
              <Stack rowGap={3}>
                {boardData.status === "rejected" && (
                  <ErrorDialog open={true} />
                )}
                {(boardData.status === "pending" && <Loader />) ||
                  (boardData.status === "fulfilled" && (
                    <TrelloBoard boards={boardData.data} status={boardData.status } />
                  ))}
              </Stack>
            }
          />
          <Route path="/boards/:id" element={<BoardLists />} />
          <Route path="*" element={<ErrorPage />} />
        </Routes>
      </SnackbarProvider>
    </>
  );
}

export default App;
