import axios from "axios";
const { VITE_KEY, VITE_TOKEN } = import.meta.env;

export const Url = (mainUrlPath) => {
  return `https://api.trello.com/1/${mainUrlPath}key=${VITE_KEY}&token=${VITE_TOKEN}`;
};
