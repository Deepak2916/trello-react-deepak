import { configureStore } from "@reduxjs/toolkit";
import boardSliceReducer from "./boardReducer.js";
import listSliceReducer from "./listReducer.js";
import cardSliceReducer from "./cardReducer.js";
import checkListSliceReducer from "./checkListReducer.js";
const store = configureStore({
  reducer: {
    board: boardSliceReducer,
    list: listSliceReducer,
    card: cardSliceReducer,
    checkList: checkListSliceReducer,
  },
});

export default store;
