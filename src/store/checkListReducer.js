import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { Url } from "../api";
import axios from "axios";

const initialState = {
  data: [],
  isCheckListOpen: false,
  status: "",
};

export const getCheckListData = createAsyncThunk(
  "checklist/get",
  async ({ id }) => {
    const getcheckLists = await axios.get(Url(`cards/${id}/checklists?`));
    return getcheckLists.data;
  }
);

export const createCheckList = createAsyncThunk(
  "checklist/create",
  async ({ cardId, CheckListName }) => {
    const newCheckList = await axios.post(
      Url(`cards/${cardId}/checklists?name=${CheckListName}&`)
    );

    return newCheckList.data;
  }
);

export const deleteCheckList = createAsyncThunk(
  "checklist/delete",
  async (id) => {
    await axios.delete(Url(`checklists/${id}?`));
    return id;
  }
);

export const createCheckItem = createAsyncThunk(
  "checkitem/create",
  async ({ checkListId, name }) => {
    const newCheckItem = await axios.post(
      Url(`checklists/${checkListId}/checkItems?name=${name}&`)
    );

    return { checkListId: checkListId, data: newCheckItem.data };
  }
);

export const updateCheckItem = createAsyncThunk(
  "checkitem/update",
  async ({ cardId, itemId, status }) => {
    const updatedItem = await axios.put(
      Url(`cards/${cardId}/checkItem/${itemId}?`),
      {
        state: status,
      }
    );

    return updatedItem.data;
  }
);

export const deleteCheckItem = createAsyncThunk(
  "checkitem/delete",
  async ({ checkListId, itemId }) => {
    await axios.delete(Url(`checklists/${checkListId}/checkItems/${itemId}?`));
    return { checkListId, itemId };
  }
);

export const checkListSlice = createSlice({
  name: "checkList",
  initialState,
  reducers: {
    closeCheckList: (state, action) => {
      state.isCheckListOpen = false;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getCheckListData.pending, (state, action) => {
        state.status = "pending";
      })
      .addCase(getCheckListData.fulfilled, (state, action) => {
        state.data = action.payload;
        state.status = "fulfilled";
        state.isCheckListOpen = true;
      })
      .addCase(getCheckListData.rejected, (state, action) => {
        state.status = "rejected";
      })
      .addCase(createCheckList.pending, (state, action) => {
        state.status = "pending";
      })
      .addCase(createCheckList.fulfilled, (state, action) => {
        state.data.push(action.payload);
        state.status = "fulfilled";
      })
      .addCase(createCheckList.rejected, (state, action) => {
        state.status = "rejected";
      })
      .addCase(deleteCheckList.pending, (state, action) => {
        state.status = "pending";
      })
      .addCase(deleteCheckList.fulfilled, (state, action) => {
        state.status = "fulfilled";
        state.data = state.data.filter((list) => {
          return list.id !== action.payload;
        });
      })
      .addCase(deleteCheckList.rejected, (state, action) => {
        state.status = "rejected";
      })
      .addCase(createCheckItem.pending, (state, action) => {
        state.status = "pending";
      })
      .addCase(createCheckItem.fulfilled, (state, action) => {
        state.status = "fulfilled";
        const newData = state.data.map(({ ...checkList }) => {
          if (checkList.id == action.payload.checkListId) {
            checkList.checkItems = [
              ...checkList.checkItems,
              action.payload.data,
            ];
          }
          return checkList;
        });

        state.data = newData;
      })
      .addCase(createCheckItem.rejected, (state) => {
        state.status = "rejected";
      })
      .addCase(updateCheckItem.pending, (state) => {
        state.status = "pending";
      })
      .addCase(updateCheckItem.fulfilled, (state, action) => {
        state.status = "fulfilled";
  
        const updatedData = state.data.map((data) => {
          if (data.id == action.payload.idChecklist) {
            data.checkItems = data.checkItems.map((item) => {
              if (item.id == action.payload.id) {
                item.state = action.payload.state;
              }
              return { ...item };
            });
          }
          return data;
        });
        state.data = updatedData;
      })
      .addCase(updateCheckItem.rejected, (state, action) => {
        state.status = "rejected";
      })
      .addCase(deleteCheckItem.pending, (state, action) => {
        state.status = "pending";
      })
      .addCase(deleteCheckItem.fulfilled, (state, action) => {
        state.status = "fulfilled";
        const newData = state.data.map((data) => {
          if (data.id == action.payload.checkListId) {
            data.checkItems = data.checkItems.filter((item) => {
              return item.id !== action.payload.itemId;
            });
          }
          return data;
        });
        state.data = [...newData];
      })
      .addCase(deleteCheckItem.rejected, (state, action) => {
        state.status = "rejected";
      });
  },
});

export const { closeCheckList } = checkListSlice.actions;
export default checkListSlice.reducer;
