import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { Url } from "../api";
import axios from "axios";
const initialState = {
  data: [],
  status: "",
};

export const getCardData = createAsyncThunk("card/getcards", async (id) => {
  const getAllCardsInBoardById = await axios.get(Url(`boards/${id}/cards?`));

  return getAllCardsInBoardById.data;
});

export const createCard = createAsyncThunk(
  "card/createcard",
  async ({ listId, cardName }) => {
    const newCard = await axios.post(
      Url(`cards?idList=${listId}&name=${cardName}&`)
    );

    return newCard.data;
  }
);

export const deleteCard = createAsyncThunk("card/deletecard", async (id) => {
  await axios.delete(Url(`cards/${id}?`));

  return id;
});

export const cardSlice = createSlice({
  name: "card",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getCardData.pending, (state, action) => {
        state.status = "pending";
      })
      .addCase(getCardData.fulfilled, (state, action) => {
        state.data = action.payload;
        state.status = "fulfilled";
      })
      .addCase(getCardData.rejected, (state, action) => {
        state.status = "rejected";
      })
      .addCase(createCard.pending, (state, action) => {
        state.status = "pending";
      })
      .addCase(createCard.fulfilled, (state, action) => {
        state.data.push(action.payload);
        state.status = "fulfilled";
      })
      .addCase(createCard.rejected, (state, action) => {
        state.status = "rejected";
      })
      .addCase(deleteCard.pending, (state, action) => {
        state.status = "pending";
      })
      .addCase(deleteCard.fulfilled, (state, action) => {
        state.data = state.data.filter((list) => {
          return list.id !== action.payload;
        });
        state.status = "fulfilled";
      })
      .addCase(deleteCard.rejected, (state, action) => {
        state.status = "rejected";
      });
  },
});

export default cardSlice.reducer;
