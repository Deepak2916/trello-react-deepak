import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { Url } from "../api";
import axios from "axios";

const initialState = {
  data: [],
  status: "",
};
export const fetchBoardData = createAsyncThunk("boards/getData", async () => {
  const response = await axios.get(Url("members/me/boards?"));
  return response.data;
});

export const createBoard = createAsyncThunk(
  "boards/createBoard",
  async (newBoardName) => {
    const newBoard = await axios.post(Url(`boards/?name=${newBoardName}&`));

    return newBoard.data;
  }
);
const boardSlice = createSlice({
  name: "board",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchBoardData.pending, (state) => {
        state.status = "pending";
      })
      .addCase(fetchBoardData.fulfilled, (state, action) => {
        state.data = action.payload;
        state.status = "fulfilled";
      })
      .addCase(fetchBoardData.rejected, (state) => {
        state.status = "rejected";
      })
      .addCase(createBoard.pending, (state, action) => {
        state.status = "pending";
      })
      .addCase(createBoard.fulfilled, (state, action) => {
        state.data.push(action.payload);
        state.status = "fulfilled";
      })
      .addCase(createBoard.rejected, (state) => {
        state.status = "rejected";
      });
  },
});

export default boardSlice.reducer;
