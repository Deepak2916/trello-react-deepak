import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
// import { httpsRequest } from "../api";
import { Url } from "../api";
import axios from "axios";
const initialState = {
  data: [],
  status: "",
};

export const getListData = createAsyncThunk("list/getlists", async (id) => {

  const getBoardListsByBoardId = await axios.get(Url(`boards/${id}/lists?`));

  return getBoardListsByBoardId.data;
});

export const createList = createAsyncThunk(
  "list/createlist",
  async ({ id, listName }) => {
    const newList = await axios.post(
      Url(`boards/${id}/lists?name=${listName}&`)
    );

    return newList.data;
  }
);

export const deleteList = createAsyncThunk("list/deletelist", async (id) => {
  await axios.put(Url(`lists/${id}?closed=true&`));
  return id;
});
const listSlice = createSlice({
  name: "lists",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getListData.pending, (state) => {
        state.status = "pending";
      })
      .addCase(getListData.fulfilled, (state, action) => {
        state.status = "fulfilled";
        state.data = action.payload;
      })
      .addCase(getListData.rejected, (state) => {
        state.status = "rejected";
      })
      .addCase(createList.pending, (state) => {
        state.status = "pending";
      })
      .addCase(createList.fulfilled, (state, action) => {
        state.status = "fulfilled";
        state.data.push(action.payload);
      })
      .addCase(createList.rejected, (state) => {
        state.status = "rejected";
      })
      .addCase(deleteList.fulfilled, (state, action) => {
        state.data = state.data.filter((list) => {
          return list.id !== action.payload;
        });
      });
  },
});

export default listSlice.reducer;
