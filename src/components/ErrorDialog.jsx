import React, { useState } from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";
const ErrorDialog = () => {
  const [open,setOpen] = useState(true)
  function onCloseHandler(){
    setOpen(false)
  }
  return (
    <Dialog open={open} onClose={onCloseHandler}>
      <DialogTitle>Something Went Wrong</DialogTitle>
      <DialogContent>
        <DialogContentText>
          An error occurred while processing your request. Please try again
          later.
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={onCloseHandler} color="primary">
          Close
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ErrorDialog;
