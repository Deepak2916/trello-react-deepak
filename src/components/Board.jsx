import React from "react";
import { Card, Typography } from "@mui/material";
import { styled } from "@mui/material/styles";
import CreateAndDeletePopOver from "./CreateAndDeletePopover";
import { useNavigate } from "react-router-dom";

import { useDispatch, useSelector } from "react-redux";
import { createBoard } from "../store/boardReducer.js";
import { enqueueSnackbar } from "notistack";

const CustomStyleCard = styled(Card)(({ theme }) => ({
  width: "300px",
  height: "150px",
  backgroundSize: "cover",
  backgroundRepeat: "no-repeat",
  backgroundPosition: "center",
  "& .MuiCardContent-root": {
    backgroundColor: "rgba(255, 255, 255, 0.7)",
    border: "2px solid",
    padding: "0",
  },
}));

const Board = ({ board, areBoardsLeft}) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  // const status = useSelector((state) => {
  //   return state.board.status;
  // });
  const createNewBoard =  (newBoardName) => {

    dispatch(createBoard(newBoardName));

  };

  //   enqueueSnackbar('Created Board Successfully!', { variant: 'success' });
 

  return (
    <CustomStyleCard
      sx={{
        backgroundImage: `url(${"https://images.pexels.com/photos/625219/pexels-photo-625219.jpeg?auto=compress&cs=tinysrgb&w=1600"})`,
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "hsl(220,12%,95.1%)",
      }}
    >
      <Typography
        variant="h5"
        sx={{
          fontWeight: "bold",
          color: `${(board && "#FFE17B") || "black"}`,
          textShadow: "2px 2px 4px rgba(0, 0, 0, 0.5)",
        }}
      >
        {(board && board.name) ||
          (areBoardsLeft && (
            <CreateAndDeletePopOver
              title={`create new Board`}
              type="create"
              onSubmit={createNewBoard}
              buttonName="create"
            />
          )) || <Typography>Board Limit Exceeded </Typography>}
      </Typography>
    </CustomStyleCard>
  );
};

export default Board;
