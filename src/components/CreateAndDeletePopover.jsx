import {
  Button,
  CardContent,
  Popover,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import React, { useState } from "react";

const CreateAndDeletePopOver = ({ title, type, buttonName, onSubmit, id }) => {
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClose = () => {
    setAnchorEl(null);
  };

  const onSubmitHandler = (event) => {
    event.preventDefault();
    if (type == "delete") {
      onSubmit(id);
    } else if (event.target.name.value !== "") {
      onSubmit(event.target.name.value.trim());
    }
    setAnchorEl(null);
  };

  return (
    <Stack>
      <Button
        variant="outlined"
        sx={{ color: "black",fontWeight:'bold' }}
        onClick={(event) => {
          setAnchorEl(event.target);
        }}
      >
        {title}
      </Button>
      <Popover
        open={Boolean(anchorEl)}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{ vertical: "center", horizontal: "center" }}
      >
        <CardContent>
          <form
            onSubmit={onSubmitHandler}
            style={{
              display: "flex",
              flexDirection: "column",
              rowGap: "1rem",
            }}
          >
            {(type.includes("delete") && (
              <Typography variant="h6" p={2} gutterBottom>
                Are you sure you want to delete?
              </Typography>
            )) || (
              <TextField
                label={title}
                variant="outlined"
                fullWidth
                id="name"
                placeholder="Enter Text"
              />
            )}

            <Stack
              sx={{
                flexDirection: "row",
                justifyContent: "space-around",
                rowGap: "4rem",
              }}
            >
              <Button
                variant="text"
                color="primary"
                onClick={() => {
                  setAnchorEl(null);
                }}
              >
                cancel
              </Button>
              <Button
                type="submit"
                variant={"contained"}
                color={(type.includes("delete") && "error") || "primary"}
              >
                {buttonName}
              </Button>
            </Stack>
          </form>
        </CardContent>
      </Popover>
    </Stack>
  );
};

export default CreateAndDeletePopOver;
