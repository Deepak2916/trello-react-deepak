import React from "react";

import Button from "@mui/material/Button";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import DialogTitle from "@mui/material/DialogTitle";
import Dialog from "@mui/material/Dialog";
import Typography from "@mui/material/Typography";
import CheckBoxOutlinedIcon from "@mui/icons-material/CheckBoxOutlined";

import CreateAndDeletePopOver from "../CreateAndDeletePopover";
import CardCheckLists from "./CardCheckLists";
import ErrorDialog from "../ErrorDialog";

import {
  getCheckListData,
  createCheckList,
  closeCheckList,


} from "../../store/checkListReducer";
import { useDispatch, useSelector } from "react-redux";
import { enqueueSnackbar } from "notistack";

// import { enqueueSnackbar } from "notistack";

function CustomCheckListDialogBox(props) {
  const { onClose, open, card } = props;
  const dispatch = useDispatch();

  const checkLists = useSelector((state) => {
    return state.checkList;
  });

  const handleClose = () => {
    onClose();
  };

  const createNewCheckList = async (CheckListName) => {
 
    //     enqueueSnackbar("Created CheckList Successfully!", {
    //       variant: "success",
    //     });
      dispatch(createCheckList({cardId:card.id,CheckListName}));

  };


  if (checkLists.status === 'rejected') {
    return <ErrorDialog />;
  }

  return (
    <Dialog onClose={handleClose} open={checkLists.isCheckListOpen || false}>
      <DialogTitle
        sx={{
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <Typography
          sx={{
            fontSize: "20px",
          }}
        >
          {card && card.name}
        </Typography>
        <Button
          variant="text"
          onClick={() => {
            onClose();
          }}
          sx={{
            fontSize: "20px",
            color: "black",
          }}
        >
          X
        </Button>
      </DialogTitle>
      <List
        sx={{
          padding: "1rem",
        }}
      >
        <Typography variant="h5">Checklists</Typography>

        <ListItem
          sx={{
            display: "flex",
            flexDirection: "column",
            rowGap: "1rem",
          }}
        >
          <ListItemButton autoFocus>
            <CheckBoxOutlinedIcon />

            <CreateAndDeletePopOver
              title="create checkList"
              type="create"
              onSubmit={(name) => {
                createNewCheckList(name);
              }}
              buttonName="create"
            />
          </ListItemButton>

          <CardCheckLists cardId={card.id} />
        </ListItem>
      </List>
    </Dialog>
  );
}

export default function CheckListDialogBox({ selectedCard }) {
  const dispatch = useDispatch();
  const getCheckListsByCardId =  () => {

      dispatch(getCheckListData({id:selectedCard.id}));
  
  };

  const handleClose = () => {
    dispatch(closeCheckList());
  };

  return (
    <div>
      <Button
        onClick={() => {
          getCheckListsByCardId();
        }}
        sx={{
          margin: "0.2rem",
          color: "black",
          alignSelf: "stretch",
          width: "150px",
        }}
      >
        {selectedCard.name}
      </Button>

      <CustomCheckListDialogBox onClose={handleClose} card={selectedCard} />
    </div>
  );
}
