import React from "react";
import Box from "@mui/material/Box";
import Checkbox from "@mui/material/Checkbox";
import FormControlLabel from "@mui/material/FormControlLabel";
import ProgressBar from "./ProgressBar";

import { Stack, Typography } from "@mui/material";

import CreateAndDeletePopOver from "../CreateAndDeletePopover";
import {
  createCheckItem,
  updateCheckItem,
  deleteCheckItem,
} from "../../store/checkListReducer";
import { useDispatch } from "react-redux";
import { enqueueSnackbar } from "notistack";

export default function Items({ items, checkListId, cardId }) {
  const dispatch = useDispatch();

  const handleCreateCheckItem = async (name) => {

    dispatch(
      createCheckItem({
        name,
        checkListId,
      })
    );
    //     enqueueSnackbar("Created ChecItem Successfully!", { variant: "success" });
 
  };
  const handleDeleteCheckItem = async (itemId) => {

    dispatch(
      deleteCheckItem({
        checkListId: checkListId,
        itemId: itemId,
      })
    );
    //     enqueueSnackbar("Deleted CheckItem Successfully!", {
  
  };
  const handleUpdateCheckItem = (event) => {
    let state = (event.target.checked && "complete") || "incomplete";

    dispatch(
      updateCheckItem({
        cardId,
        itemId: event.target.id,
        status: state,
      })
    );
    //   enqueueSnackbar("Updated Item Successfully!", { variant: "success" });
  };

  return (
    <Stack
      key={checkListId}
      p={1}
      sx={{
        rowGap: "0.5rem",
      }}
    >
      <ProgressBar checkItems={items} />
      <Box
        sx={{
          display: "flex",
          rowGap: "0.5rem",
          flexDirection: "column",
        }}
      >
        {items &&
          items.map((item) => {
            return (
              <Stack
                key={item.id}
                sx={{
                  alignSelf: "stretch",
                  flexDirection: "row",
                  rowGap: "0.5rem",
                  alignItems: "center",
                  justifyContent: "space-around",
                  backgroundColor: "hsl(120,4.76%,95.88%)",
                }}
              >
                <FormControlLabel
                  style={{ flex: "0" }}
                  key={item.id}
                  label=""
                  control={
                    <Checkbox
                      checked={item.state === "complete"}
                      id={item.id}
                      onChange={handleUpdateCheckItem}
                    />
                  }
                />
                <Typography
                  variant="body1"
                  sx={{
                    flex: "1",
                    overflow: "hidden",
                    textOverflow: "ellipsis",
                  }}
                >
                  {item.name}
                </Typography>

                <CreateAndDeletePopOver
                  title="Delete"
                  type="delete"
                  id={item.id}
                  onSubmit={handleDeleteCheckItem}
                  buttonName="Delete"
                />
              </Stack>
            );
          })}
        <Stack>
          <CreateAndDeletePopOver
            title="+ Add CheckItem"
            type="create"
            onSubmit={handleCreateCheckItem}
            buttonName="Add"
          />
        </Stack>
      </Box>
    </Stack>
  );
}
