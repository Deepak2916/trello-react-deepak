import { Card } from "@mui/material";

import CreateAndDeletePopOver from "../CreateAndDeletePopover";
import CheckListDialogBox from "./CheckListDialogBox";
import { useSelector, useDispatch } from "react-redux";
import { createCard, deleteCard } from "../../store/cardReducer";
import ErrorDialog from "../ErrorDialog";
import { enqueueSnackbar } from "notistack";

const ListCards = ({ listId }) => {
  const cards = useSelector((state) => {
    return state.card;
  });

  const dispatch = useDispatch();

  const createNewCard =  (cardName) => {
      dispatch(createCard({ listId,cardName}));

    //   enqueueSnackbar('Created card Successfully!', { variant: 'success' });

  };
  const deleteListCard = async (id) => {
      dispatch(deleteCard(id));
 
    //   enqueueSnackbar('Deletd card Successfully!', { variant: 'success' });

  };

  return (
    <>
      {cards.data
        .filter((card) => {
          return card.idList === listId;
        })
        .map((card, index) => (
          <Card
            key={index}
            sx={{
              margin: "0",
              width: "100%",
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <CheckListDialogBox
              selectedCard={{ name: card.name, id: card.id }}
            />

            <CreateAndDeletePopOver
              title={`Delete`}
              type="delete"
              onSubmit={deleteListCard}
              buttonName="Delete"
              id={card.id}
            />
          </Card>
        ))}

      <CreateAndDeletePopOver
        title="+ Add a card"
        type="create"
        onSubmit={createNewCard}
        buttonName="Add"
      />
    </>
  );
};

export default ListCards;
