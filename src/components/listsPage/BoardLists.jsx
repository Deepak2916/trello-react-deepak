import React, { useEffect } from "react";
import { useLocation, useParams } from "react-router-dom";
import { Stack, Typography } from "@mui/material";
import ListCards from "./ListCards";

import Loader from "../Loader";
import CreateAndDeletePopOver from "../CreateAndDeletePopover";
import ErrorDialog from "../ErrorDialog";
import {
  getListData,
  createList,
  deleteList,

} from "../../store/listReducer";
import { getCardData } from "../../store/cardReducer";
import { useSelector, useDispatch } from "react-redux";
import { enqueueSnackbar } from "notistack";

const customStackStyle = {
  backgroundColor: "hsl(220,12%,95.1%)",
  padding: "10px",
  borderRadius: "1rem",
  rowGap: "0.5rem",
  width: "250px",
  flexShrink: 0,
};
const BoardLists = () => {
  const { id } = useParams();

  const lists = useSelector((state) => {
    return state.list;
  });

  const dispatch = useDispatch();

  const location = useLocation();

  const { boardName } = location.state;

  useEffect(() => {
  
      dispatch(getListData(id));
    
        dispatch(getCardData(id));
    
  }, []);

  const createBoardList = async (listName) => {

    
    dispatch(createList({id,listName}))
  
    //   enqueueSnackbar('Created list Successfully!', { variant: 'success' });

  };

  const deleteBoardList = async (id) => {
  
    dispatch(deleteList(id));
  
    //   enqueueSnackbar('Deleted list Successfully!', { variant: 'success' });
 
  };

  if (lists.status==='rejected') {
    return <ErrorDialog open={true} onClose={errorHandleClose} />;
  }
  if (lists.status==='pending') {
    return <Loader />;
  }

  return (
    <>
      <Typography
        variant="h5"
        p={1}
        sx={{
          backgroundColor: "hsl(201.98,100%,19.8%)",
          color: "white",
          fontWeight: "bold",
        }}
      >
        {boardName}
      </Typography>
      <Stack
        direction="row"
        columnGap={2}
        p={4}
        alignItems="start"
        sx={{
          overflowX: "scroll",
          minHeight: "85vh",
          backgroundImage:
            "url(https://images.pexels.com/photos/625219/pexels-photo-625219.jpeg?auto=compress&cs=tinysrgb&w=1600)",
          backgroundPosition: "top",
        }}
      >
        {lists.data &&
          lists.data.map((list) => {
            return (
              <Stack
                key={list.id}
                sx={{
                  ...customStackStyle,
                }}
              >
                <Stack
                  sx={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  <Typography variant="h5">{list.name}</Typography>

                  <CreateAndDeletePopOver
                    title="Dlete"
                    type="delete"
                    onSubmit={deleteBoardList}
                    id={list.id}
                    buttonName="Delete"
                  />
                </Stack>

                <ListCards key={list.id} listId={list.id} />
              </Stack>
            );
          })}

        <Stack
          sx={{
            ...customStackStyle,
          }}
        >
          <CreateAndDeletePopOver
            title="+ Add another list"
            type="create"
            onSubmit={createBoardList}
            buttonName="Add"
          />
        </Stack>
      </Stack>
    </>
  );
};

export default BoardLists;
