import React from "react";
import CreateAndDeletePopOver from "../CreateAndDeletePopover";
import { Stack, Typography } from "@mui/material";
import Items from "./Items";
import { useSelector, useDispatch } from "react-redux";
import { deleteCheckList } from "../../store/checkListReducer";
import { enqueueSnackbar } from "notistack";

const CardCheckLists = ({ cardId }) => {
  const checkLists = useSelector((state) => {
    return state.checkList;
  });

  const dispatch = useDispatch();

  const deleteCheckListById = async (id) => {

      dispatch(deleteCheckList(id));
    //   enqueueSnackbar('Deleted checklist Successfully!', { variant: 'success' });
 
  };

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        rowGap: "0.5rem",
        alignSelf: "stretch",
      }}
    >
      {checkLists.data &&
        checkLists.data.map((checkList) => {
          return (
            <Stack
              key={checkList.id}
              style={{
                paddingBottom: "1rem",
                boxShadow: "0 0 10px gray",
              }}
            >
              <Stack
                sx={{
                  flexDirection: "row",
                  boxShadow: "0 0 10px gray",
                  columnGap: "4rem",

                  padding: "0.5rem",
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
              >
                <Typography variant="h5">{checkList.name}</Typography>

                <CreateAndDeletePopOver
                  title="Delete checkList"
                  type="delete"
                  onSubmit={deleteCheckListById}
                  id={checkList.id}
                  buttonName="Delete"
                />
              </Stack>

              <Items
                items={checkList.checkItems}
                checkListId={checkList.id}
                cardId={cardId}
              />
            </Stack>
          );
        })}
    </div>
  );
};

export default CardCheckLists;
