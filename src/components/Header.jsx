import React from "react";

import { AppBar, IconButton, Toolbar } from "@mui/material";
import { Link } from "react-router-dom";

const Header = () => {
  return (
    <AppBar
      sx={{
        background: "white",
        position: "relative",
        width: "100%",
      }}
    >
      <Toolbar>
        <Link to={"/"}>
          <IconButton sx={{ mr: 2 }}>
            <img
              width="80px"
              height="40px"
              src="https://1000logos.net/wp-content/uploads/2021/05/Trello-logo.png "
              alt="Trello Logo"
            />
          </IconButton>
        </Link>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
