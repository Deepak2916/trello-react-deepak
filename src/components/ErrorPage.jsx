import React from "react";
import { Link } from "react-router-dom";
import { Container, Typography, Button } from "@mui/material";

const ErrorPage = ({ status, message }) => {
  return (
    <>
      <Container
        maxWidth="sm"
        style={{ textAlign: "center", marginTop: "100px" }}
      >
        <Typography variant="h1" gutterBottom>
          {status || 404}
        </Typography>
        <Typography variant="h4" gutterBottom>
          {message || "Oops! Page not found."}
        </Typography>
        <Typography variant="body1" gutterBottom>
          The page you are looking for might be unavailable or does not exist.
        </Typography>
        <Button component={Link} to="/" variant="contained" color="primary">
          Go to Home
        </Button>
      </Container>
    </>
  );
};

export default ErrorPage;
