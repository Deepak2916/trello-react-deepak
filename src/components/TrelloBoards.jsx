import React from "react";
import { Grid } from "@mui/material";
import Board from "./Board.jsx";
import { Link } from "react-router-dom";

const TrelloBoard = ({ boards }) => {
  return (
    <Grid
      container
      spacing={0}
      sx={{
        columnGap: "100px",
        padding: "4rem",
        rowGap: "2rem",
      }}
    >
      {boards.map((board) => {
        return (
          <Link
            key={board.id}
            to={`/boards/${board.id}`}
            state={{ boardName: board.name }}
          >
            <Board key={board.id} board={board} />
          </Link>
        );
      })}
      <Board key="new" board={null} areBoardsLeft={boards.length < 10} />
    </Grid>
  );
};

export default TrelloBoard;
